
Guestbook Views - Provides views module integration for the Guestbook module.

Requirements
--------------------------------------------------------------------------------
- This module is written for Drupal 6.0+.

- The Views module.
- The Guestbook module.

Installation
--------------------------------------------------------------------------------
Copy the Guestbook Views module folder to your module directory and then enable
on the admin modules page.

USAGE
--------------------------------------------------------------------------------
Have fun creating your views.

Maintainer
--------------------------------------------------------------------------------
Quiptime Group
Siegfried Neumann
www.quiptime.com
quiptime [ at ] gmail [ dot ] com