<?php

/**
 * @file field_guestbook_views_entry.inc
 */

/**
 * Views field handler for the Guestbook Views entry.
 *
 * @ingroup views
 */
class field_guestbook_views_entry extends views_handler_field {
  function query() {
    if (!isset($this->view->field['author'])) {
      $this->query->add_field('guestbook', 'author');
    }
    if (!isset($this->view->field['anonname'])) {
      $this->query->add_field('guestbook', 'anonname');
    }

    $this->query->add_field('guestbook', 'message');

    // Entry comment.
    $this->query->add_field('guestbook', 'comment');
    $this->query->add_field('guestbook', 'commentauthor');

    $sql = "SELECT u.name AS commentauthor_name FROM {users} u WHERE u.uid = guestbook.commentauthor";
    $this->ensure_my_table();
    $this->field_alias = 'commentauthor_name';
    $this->query->add_field(NULL, "(". $sql .")", $this->table_alias .'_'. $this->field_alias);

    // Get the author realname of an entry comment.
    if (module_exists('realname')) {
      $sql2 = "SELECT rn.realname AS commentauthor_realname FROM {realname} rn WHERE rn.uid = guestbook.commentauthor";
      $this->ensure_my_table();
      $this->query->add_field(NULL, "(". $sql2 .")", $this->table_alias .'_commentauthor_realname');
    }
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['truncate_entry'] = array('default' => 50, 'translatable' => TRUE);
    $options['entry_comment'] = array('default' => 1, 'translatable' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['truncate_entry'] = array(
      '#type' => 'select',
      '#title' => t('Truncate entry'),
      '#description' => t('Truncates the entries. Please select the length to be shortened the entries. Use 0 for no truncating.'),
      '#options' => _guestbook_views_entry_truncate_options(),
      '#default_value' => $this->options['truncate_entry'],
    );
    $form['entry_comment'] = array(
      '#type' => 'select',
      '#title' => t('Entry comment'),
      '#description' => t('Display the comment of an entry.'),
      '#options' => array(
        1 => t('Yes'),
        0 => t('No'),
      ),
      '#default_value' => $this->options['entry_comment'],
    );
  }

  function element_type() {
    return 'div';
  }

  function render($values) {
    $guestbook_mode = variable_get('guestbook_mode', GUESTBOOK_SITE_GUESTBOOK | GUESTBOOK_USER_GUESTBOOKS);
    $no_entries = t('No guestbook entries available.');

    // No guestbook entries exist but guestbook is enabled.
    // Site and user guestbooks.
    if ($guestbook_mode == 3 && !isset($values->guestbook_author) && !isset($values->guestbook_anonname)) {
      return '<div class="view-empty">'. $no_entries .'</div>';
    }
    // Site guestbook only.
    if ($guestbook_mode == 1 && !isset($values->guestbook_author) && !isset($values->guestbook_anonname)) {
      return '<div class="view-empty">'. $no_entries .'</div>';
    }
    // User guestbooks only.
    if ($guestbook_mode == 2 && $values->guestbook_author == FALSE && !isset($values->guestbook_anonname)) {
      return '<div class="view-empty">'. $no_entries .'</div>';
    }

    $entry = array();

    $entry['message'] = $values->guestbook_message;
    $entry['truncate_lenght'] = $this->options['truncate_entry'];
    $entry['display_comment'] = $this->options['entry_comment'];
    $entry['comment'] = $values->guestbook_comment;
    $entry['commentauthor'] = $values->guestbook_commentauthor;
    $entry['commentauthor_name'] = $values->guestbook_commentauthor_name;
    $entry['commentauthor_realname'] = $values->guestbook_commentauthor_realname;

    return _field_guestbook_create_entry($entry);
  }
}

/**
 * Create the output of an guestbook entry itself.
 *
 * @param array $entry
 *   The array contains various user data..
 * @return string
 *   The guestbook entry.
 */
function _field_guestbook_create_entry($entry) {
  $output = '';
  $m = '';

  // Fake an account object.
  $commentauthor = new stdClass();
  $commentauthor->uid = $entry['commentauthor'];
  $commentauthor->name = $entry['commentauthor_name'];
  if ($entry['commentauthor_realname'] != FALSE) {
    $commentauthor->realname = $entry['commentauthor_realname'];
  }

  // Build the entry.
  $output .= '<div class="content guestbook-message">';
  if ($entry['truncate_lenght'] > 0) {
    $message = guestbook_views_entry_truncate($entry['message'], $entry['truncate_lenght']);
    $output .= check_markup($message, variable_get('guestbook_input_format', 1), FALSE);
  }
  else {
    $output .= check_markup($entry['message'], variable_get('guestbook_input_format', 1), FALSE);
  }
  $output .= '</div>';

  // The entry comment.
  if ($entry['display_comment'] == TRUE && $entry['commentauthor'] > 0) {
    $output .= '<div class="guestbook-comment content clear-block">';
    $output .= '<small>'. t('Comment by') .' '. theme('username', $commentauthor) .'</small><br />';
    $output .= '<em>'. $entry['comment'] .'</em>';
    $output .= '</div>';
  }

  return $output;
}
