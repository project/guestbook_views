<?php

/**
 * @file field_guestbook_views_entry_author.inc
 */

/**
 * Views field handler for the Guestbook Views entry author link.
 *
 * @ingroup views
 */
class field_guestbook_views_entry_author extends views_handler_field_user_link {
  function query() {
    if (!isset($this->view->field['author'])) {
      $this->query->add_field('guestbook', 'author');
    }
    if (!isset($this->view->field['anonname'])) {
      $this->query->add_field('guestbook', 'anonname');
    }

    $sql = "SELECT u.name AS author_name FROM {users} u WHERE u.uid = guestbook.author";
    $this->ensure_my_table();
    $this->field_alias = 'entry_author_name';
    $this->query->add_field(NULL, "(". $sql .")", $this->table_alias .'_'. $this->field_alias);

    // Get the author realname of an entry.
    if (module_exists('realname')) {
      $sql2 = "SELECT rn.realname AS author_realname FROM {realname} rn WHERE rn.uid = guestbook.author";
      $this->ensure_my_table();
      $this->query->add_field(NULL, "(". $sql2 .")", $this->table_alias .'_entry_author_realname');
    }
  }

  function element_type() {
    return 'div';
  }

  function render($values) {
    $guestbook_mode = variable_get('guestbook_mode', GUESTBOOK_SITE_GUESTBOOK | GUESTBOOK_USER_GUESTBOOKS);

    // No guestbook entries exist but guestbook is enabled.
    // Site and user guestbooks.
    if ($guestbook_mode == 3 && !isset($values->guestbook_author) && !isset($values->guestbook_anonname)) {
      return;
    }
    // Site guestbook only.
    if ($guestbook_mode == 1 && !isset($values->guestbook_author) && !isset($values->guestbook_anonname)) {
      return;
    }
    // User guestbooks only.
    if ($guestbook_mode == 2 && $values->guestbook_author == FALSE && !isset($values->guestbook_anonname)) {
      return;
    }

    if ($values->guestbook_author > 0) {
      // Fake an account object.
      $account = new stdClass();
      $account->uid = $values->guestbook_author;
      $account->name = $values->guestbook_entry_author_name;
      if ($values->guestbook_entry_author_realname) {
        $account->realname = $values->guestbook_entry_author_realname;
      }

      $author =  _field_guestbook_create_entry_author($values, $account);
    }
    elseif ($values->guestbook_author == 0) {
      // Anonymous entry.
      if (!empty($values->guestbook_anonname)) {
        $author = check_plain($values->guestbook_anonname);
      }
      else {
        $author = variable_get('anonymous', t('Anonymous'));
      }
    }

    return $author;
  }
}

/**
 * Create the link to entry author account.
 *
 * @param object $values
 * @param object $account
 *   The user to build the link.
 * @return string
 *   The rendered link of entry author.
 */
function _field_guestbook_create_entry_author(&$values, $account) {
  return theme('username', $account);
}
