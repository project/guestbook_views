<?php

/**
 * @file field_guestbook_views_entry_author_picture.inc
 */

/**
 * Views field handler for the Guestbook Views entry author picture.
 *
 * @ingroup views
 */
class field_guestbook_views_entry_author_picture extends views_handler_field_user_picture {
  function query() {
    $this->query->add_field('guestbook', 'author');

    if (!isset($this->view->field['anonname'])) {
      $this->query->add_field('guestbook', 'anonname');
    }

    $sql = "SELECT u.mail AS author_mail FROM {users} u WHERE u.uid = guestbook.author";
    $sql2 = "SELECT u.picture AS author_picture FROM {users} u WHERE u.uid = guestbook.author";

    $this->ensure_my_table();

    $this->query->add_field(NULL, "(". $sql .")", $this->table .'_entry_author_mail');
    $this->query->add_field(NULL, "(". $sql2 .")", $this->table .'_entry_author_picture');
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['imagecache_preset'] = array(
      'default' => variable_get('user_picture_imagecache_profiles_default', ''),
      'translatable' => FALSE
    );
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $presets = imagecache_presets();
    $opt = array('' => '');
    foreach ($presets as $preset) {
      $opt[$preset['presetname']] = check_plain($preset['presetname']);
    }
    $options = $this->options;
    $form['imagecache_preset'] = array(
      '#title' => t('Imagecache preset'),
      '#type'  => 'select',
      '#options' => $opt,
      '#default_value' => $options['imagecache_preset'],
    );
  }

  function render($values) {
    $guestbook_mode = variable_get('guestbook_mode', GUESTBOOK_SITE_GUESTBOOK | GUESTBOOK_USER_GUESTBOOKS);

    // No guestbook entries exist but guestbook is enabled.
    // Site and user guestbooks.
    if ($guestbook_mode == 3 && !isset($values->guestbook_author) && !isset($values->guestbook_anonname)) {
      return;
    }
    // Site guestbook only.
    if ($guestbook_mode == 1 && !isset($values->guestbook_author) && !isset($values->guestbook_anonname)) {
      return;
    }
    // User guestbooks only.
    if ($guestbook_mode == 2 && $values->guestbook_author == FALSE && !isset($values->guestbook_anonname)) {
      return;
    }

    $options = $this->options;

    // Fake an account object.
    $account = new stdClass();
    $account->uid = $values->guestbook_author;
    $account->name = isset($values->guestbook_entry_author_name) ? $values->guestbook_entry_author_name : variable_get('anonymous', t('Anonymous'));
    $account->mail = $values->guestbook_entry_author_mail;
    $account->picture = $values->guestbook_entry_author_picture;

    if ($options['imagecache_preset']) {
      $account->imagecache_preset = $options['imagecache_preset'];
    }

    return _field_guestbook_create_entry_author_picture($account);
  }
}

/**
 * Create the picture of an entry author.
 *
 * @param object $account
 *   The user object of the user to get the picture.
 * @return string
 *   The rendered link of entry author.
 */
function _field_guestbook_create_entry_author_picture(&$account) {
  return theme('user_picture', $account);
}
