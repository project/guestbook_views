<?php

/**
 * @file field_guestbook_views_entry_counter.inc
 */

/**
 * Views field handler for the Guestbook Views entry counter.
 *
 * @ingroup views
 */
class field_guestbook_views_entry_counter extends views_handler_field_numeric {
  function query() {
    $sql = "SELECT COUNT(guestbook.id) AS counter FROM {guestbook} WHERE users.uid = guestbook.recipient";
    $this->ensure_my_table();
    $this->query->add_field(NULL, "(". $sql .")", $this->table_alias .'_entry_counter');

    if ($this->options['split_counter_result'] > 0) {
      $sql2 = "SELECT COUNT(guestbook.id) AS counter_reg FROM {guestbook} WHERE users.uid = guestbook.recipient AND guestbook.author <> 0";
      $this->ensure_my_table();
      $this->query->add_field(NULL, "(". $sql2 .")", $this->table_alias .'_entry_counter_registered');

      $sql3 = "SELECT COUNT(guestbook.id) AS counter_anon FROM {guestbook} WHERE users.uid = guestbook.recipient AND guestbook.author = 0";
      $this->ensure_my_table();
      $this->query->add_field(NULL, "(". $sql3 .")", $this->table_alias .'_entry_counter_anon');
    }
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['split_counter_result'] = array('translatable' => TRUE);
    $options['info'] = array();

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['split_counter_result'] = array(
      '#type' => 'select',
      '#title' => t('Split counter result'),
      '#description' => t('Split the entry counter result to display a result of registered users and a result of anonymous visitors.'),
      '#options' => array(
        0 => t('Display a single result'),
        1 => t('Splitted result: x/y'),
        2 => t('Splitted result: Reg x / Anon y')
      ),
      '#default_value' => $this->options['split_counter_result'],
    );

    $form['info'] = array(
      '#type' => 'markup',
      '#value' => '<div class="form-item description">'. t('If the guestbook empty, then the counter not displayed.') .'</div>',
    );
  }

  function render($values) {
    switch ($this->options['split_counter_result']) {
      case 0:
        $counter = $values->guestbook_entry_counter;
        break;

      case 1:
        $counter = $values->guestbook_entry_counter_registered .'/'. $values->guestbook_entry_counter_anon;
        break;

      case 2:
        $counter = t('Reg') .' '. $values->guestbook_entry_counter_registered .' / '. t('Anon') .' '. $values->guestbook_entry_counter_anon;
        break;
    }

    $output = $this->options['prefix'];
    $output .= '<span class="guestbook-entry-counter">';
    $output .= ' '. $counter;
    $output .= '</span>';
    $output .= $this->options['suffix'];

    return $output;
  }
}
