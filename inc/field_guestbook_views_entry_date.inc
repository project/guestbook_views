<?php

/**
 * @file field_guestbook_views_entry_date.inc
 */

/**
 * Views field handler for the Guestbook Views entry date.
 *
 * @ingroup views
 */
class field_guestbook_views_entry_date extends views_handler_field_date {
  function query() {
    $this->query->add_field('guestbook', 'created');
  }

  function render($values) {
    if ($this->options['date_format'] != 'custom') {
      $entry_date = _field_guestbook_create_entry_date($values, $this->options['date_format']);
    }
    if ($this->options['date_format'] == 'custom') {
      $entry_date = _field_guestbook_create_entry_date($values, $this->options['date_format'], $this->options['custom_date_format']);
    }

    return $entry_date;
  }
}

/**
 * Create the date from an entry.
 *
 * @param object $values
 *   The name of user to use.
 * @param string $type
 *   The format to use. Can be "small", "medium" or "large" for the preconfigured date formats.
 *   If "custom" is specified, then $format is required as well.
 * @param string $format
 *   The format to use with custom date format.
 * @return string
 *   The rendered entry date.
 */
function _field_guestbook_create_entry_date(&$values, $type, $format = '') {
  $date = $values->guestbook_created ? format_date($values->guestbook_created, $type, $format) : theme('views_nodate');

  return $date;
}
