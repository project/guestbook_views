<?php

/**
 * @file field_guestbook_views_guestbook_status.inc
 */

/**
 * Views field handler for the Guestbook Views guestbook status.
 *
 * @ingroup views
 */
class field_guestbook_views_guestbook_status extends views_handler_field {
  function query() {
    $status_string_true = 'guestbook_status";s:1:"1';

    $sql = "(SELECT LOCATE('". $status_string_true ."', u.data) FROM {users} u WHERE u.uid = users.uid) > 0";

    $this->ensure_my_table();
    $this->field_alias = 'status';
    $this->query->add_field(NULL, "(". $sql .")", $this->table_alias .'_'. $this->field_alias);
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['return_variant'] = array('default' => 1, 'translatable' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['return_variant'] = array(
      '#type' => 'select',
      '#title' => t('Display variant'),
      '#description' => t('What should be shown the status?'),
      '#options' => array(
        1 => t('Boolean: TRUE or FALSE'),
        2 => t('String: Available or Not available'),
        3 => t('String: Enabled or Disabled'),
      ),
      '#default_value' => $this->options['return_variant'],
    );
  }

  function render($values) {
    switch ($this->options['return_variant']) {
      case 1:

        return $values->guestbook_status;

      case 2:
      case 3:
        $string_options = _guestbook_views_return_variant_string_options();

        return '<span class="guestbook-status-'. $values->guestbook_status .'">'. $string_options[$this->options['return_variant']][$values->guestbook_status] .'</span>';
    }
  }
}

/**
 * Provides the text strings for guestbook statuses.
 * 
 * @return array 
 */
function _guestbook_views_return_variant_string_options() {
  return array(
    2 => array(
      1 => t('Available'),
      0 => t('Not available')
    ),
    3 => array(
      1 => t('Enabled'),
      0 => t('Disabled')
    ),
  );
}
