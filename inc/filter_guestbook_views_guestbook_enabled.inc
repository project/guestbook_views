<?php

/**
 * @file filter_guestbook_views_guestbook_status.inc
 *
 * QTChat Views is user online filter handler.
 */

/**
 * Views filter handler for the Guestbook Views guestbook status.
 *
 * @ingroup views
 */
class filter_guestbook_views_guestbook_enabled extends views_handler_filter_boolean_operator {
  function query() {
    $status_string_true = 'guestbook_status";s:1:"1';

    $sql = "(SELECT LOCATE('". $status_string_true ."', u.data) FROM {users} u WHERE u.uid = users.uid) > 0";

    $this->ensure_my_table();
    $this->query->add_where($this->options['group'], $sql);

    $this->query->add_field('users', 'uid');
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['info'] = array();

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['info'] = array(
      '#type' => 'markup',
      '#value' => '<div class="form-item description">'. t('Static filter to check is users guestbook enabled.') .'</div>',
    );

    unset($form['expose_button']);
  }

  /**
   * Override default True/False options.
   */
  function get_value_options() {
    $this->value_options = array(1 => t('TRUE'));
    unset($this->value_value);
    unset($this->value_options);
  }
}
