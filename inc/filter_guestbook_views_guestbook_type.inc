<?php

/**
 * @file filter_guestbook_views_guestbook_type.inc
 *
 * Guestbook Views guestbook type filter handler.
 */

/**
 * Guestbook Views filter handler for the guestbook type.
 *
 * @ingroup views
 */
class filter_guestbook_views_guestbook_type extends views_handler_filter_boolean_operator {
  function query() {
    $this->ensure_my_table();
    $where = "$this->table_alias.$this->real_field ";

    // Site guestbook.
    if ($this->options['type'] == 1) {
      $where .= '= 0';
    }
    // User guestbook.
    elseif ($this->options['type'] == 2) {
      $where .= '<> 0';
    }
    $this->query->add_where($this->options['group'], $where);
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['type'] = array();
    $options['info'] = array();

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $guestbook_modes = _guestbook_views_guestbook_modes();
    $guestbook_mode = variable_get('guestbook_mode', GUESTBOOK_SITE_GUESTBOOK | GUESTBOOK_USER_GUESTBOOKS);

    $form['info'] = array(
      '#type' => 'markup',
      '#value' => '<div class="form-item description">'. t('Current guestbook mode: %mode', array('%mode' => $guestbook_modes[$guestbook_mode])) .'</div>',
    );

    $description = t('Please choose the guestbook type to filter.') .'<br />';
    $description .= t('If you are enabled %guestbook-type, then use the argument %argument.', array('%guestbook-type' => t('User guestbook'), '%argument' => t('User: Uid')));
    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('Guestbook type'),
      '#description' => $description,
      '#options' => array(
        1 => t('Site guestbook'),
        2 => t('User guestbook')
      ),
      '#default_value' => $this->options['type']
    );

    unset($form['value']);
  }

  // Don't remove this.
  function get_value_options() {}
}
