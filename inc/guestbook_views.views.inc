<?php

/**
 * @file guestbook_views.views.inc
 *
 *   Note:
 *   To respect if an author guest book disabled it is necessary to use the field "Entry".
 *   This field uses a check whether a guest book is enabled or disabled.
 */

/**
 * Implementation of hook_views_data()
 */
function guestbook_views_views_data() {
  $data = array();

  $data['guestbook']['table']['group'] = t('Guestbook');
  $data['guestbook']['table']['base'] = array(
    'field' => 'recipient',
    'title' => 'guestbook',
  );
  $data['guestbook']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'recipient',
    ),
  );

  // Fields.
  $data['guestbook']['message'] = array(
    'title' => t('Entry'),
    'help' => t('Provides an guestbook entry.'),
    'field' => array(
       'handler' => 'field_guestbook_views_entry',
     ),
  );
  $data['guestbook']['entry_author'] = array(
    'title' => t('Entry author'),
    'help' => t('Provides the author of an guestbook entry.'),
    'field' => array(
       'handler' => 'field_guestbook_views_entry_author',
       'click sortable' => TRUE,
     ),
  );
  $data['guestbook']['entry_author_picture'] = array(
    'title' => t('Entry author picture'),
    'help' => t('Provides the author picture of an guestbook entry.'),
    'field' => array(
       'handler' => 'field_guestbook_views_entry_author_picture',
     ),
  );
  $data['guestbook']['entry_date'] = array(
    'title' => t('Entry date'),
    'help' => t('Provides the date of an guestbook entriy.'),
    'field' => array(
       'handler' => 'field_guestbook_views_entry_date',
       'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'sort_guestbook_views_entry_date',
    ),
  );
  $data['guestbook']['guestbook_status'] = array(
    'title' => t('Status'),
    'help' => t('Provides the guestbook status.'),
    'field' => array(
       'handler' => 'field_guestbook_views_guestbook_status',
     ),
  );
  $data['guestbook']['entry_counter'] = array(
    'title' => t('Entry counter'),
    'help' => t('Count the entries of a guestbook.'),
    'field' => array(
       'handler' => 'field_guestbook_views_entry_counter',
     ),
  );

  // Filters.
  $data['guestbook']['guestbook_enabled'] = array(
    'group' => t('Guestbook'),
    'title' => t('Guestbook enabled'),
    'help' => t('Static filter to check is users guestbook enabled.'),
    'filter' => array(
       'handler' => 'filter_guestbook_views_guestbook_enabled',
     ),
  );
  $data['guestbook']['guestbook_disabled'] = array(
    'group' => t('Guestbook'),
    'title' => t('Guestbook disabled'),
    'help' => t('Static filter to check is users guestbook disabled.'),
    'filter' => array(
       'handler' => 'filter_guestbook_views_guestbook_disabled',
     ),
  );
  $data['guestbook']['created'] = array(
    'group' => t('Guestbook'),
    'title' => t('Entry date'),
    'help' => t('Filter guestbook entries by their date.'),
    'filter' => array(
       'handler' => 'views_handler_filter_date',
     ),
    'sort' => array(
       'handler' => 'views_handler_sort_date',
    )
  );
  $data['guestbook']['recipient'] = array(
    'group' => t('Guestbook'),
    'title' => t('Type'),
    'help' => t('Filter the guestbook by type.'),
    'filter' => array(
       'handler' => 'filter_guestbook_views_guestbook_type',
     )
  );

  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function guestbook_views_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'guestbook_views') . '/inc'
    ),
    'handlers' => array(
      'field_guestbook_views_entry' => array(
        'parent' => 'views_handler_field'
      ),
      'field_guestbook_views_entry_author' => array(
        'parent' => 'views_handler_field_user_link'
      ),
      'field_guestbook_views_entry_author_picture' => array(
        'parent' => 'views_handler_field_user_picture'
      ),
      'field_guestbook_views_entry_date' => array(
        'parent' => 'views_handler_field_date'
      ),
      'sort_guestbook_views_entry_date' => array(
        'parent' => 'views_handler_sort'
      ),
      'field_guestbook_views_guestbook_status' => array(
        'parent' => 'views_handler_field'
      ),
      'field_guestbook_views_entry_counter' => array(
        'parent' => 'views_handler_field_numeric'
      ),
      'filter_guestbook_views_guestbook_enabled' => array(
        'parent' => 'views_handler_filter_boolean_operator'
      ),
      'filter_guestbook_views_guestbook_disabled' => array(
        'parent' => 'views_handler_filter_boolean_operator'
      ),
      'filter_guestbook_views_guestbook_type' => array(
        'parent' => 'views_handler_filter_boolean_operator'
      )
    )
  );
}
