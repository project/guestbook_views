<?php

/**
 * @file sort_guestbook_views_entry_date.inc
 *
 * QTChat Views is user online sort handler.
 */

/**
 * Provides the date sortable function of the guestbook entries.
 */
class sort_guestbook_views_entry_date extends views_handler_sort {
  function query() {
    $this->ensure_my_table();
    $this->query->add_orderby(NULL, $sql, $this->options['order'], $this->table .'_created');
  }
}
